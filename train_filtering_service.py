from flask import Flask, request, jsonify,make_response
import csv
from datetime import time as datetime_time
import requests
from flask_cors import CORS

app = Flask(__name__)

CORS(app, resources={r"/*": {"origins": "*"}})

# File path for the CSV data
csv_file_path = 'train_data.csv'

# Sample data representing reservations
reservations = {}


@app.route('/')
def welcome():
    return jsonify({"message": "Welcome to the Train Filtering Service!"})


    
def load_train_data():
    with open(csv_file_path, 'r') as file:
        reader = csv.DictReader(file)
        trains = list(reader)
        return trains


@app.route('/check_database_connection', methods=['GET'])
def check_database_connection():
    # Load train data from CSV file
    global available_trains
    available_trains = load_train_data()

    return jsonify({"message": "Data loaded from CSV file", "data_from_database": available_trains})

def calculate_fare(base_fare, travel_class):
    # Define the fare multiplier based on travel class
    fare_multiplier = {'first_class': 2.0, 'premium_class': 1.5, 'standard_class': 1.0}

    # Calculate the final fare based on the class chosen
    return base_fare * fare_multiplier.get(travel_class, 1.0)


def find_return_trip_id(departure_station, arrival_station, return_date, return_time, travel_class):
    return_trip_id = None

    for train in available_trains:
        if (
            train['departure_station'] == arrival_station and
            train['arrival_station'] == departure_station and
            train['outbound_date'] == return_date and
            train['outbound_time'] == return_time
        ):
            seats_available = int(train[f'num_{travel_class.lower()}_seats'])
            if seats_available >= 1:
                return_trip_id = train['id']
                break

    return return_trip_id

def calculate_return_fares(return_trip_id, num_tickets, travel_class):
    return_trip = next((train for train in available_trains if train['id'] == return_trip_id), None)
    
    if return_trip:
        flexible_fare = calculate_fare(int(return_trip['fare_flexible_tickets']), travel_class) * num_tickets
        non_flexible_fare = calculate_fare(int(return_trip['fare_non_flexible_tickets']), travel_class) * num_tickets
        return flexible_fare, non_flexible_fare
    else:
        return None, None

@app.route('/filter_trains', methods=['POST', 'OPTIONS'])
def filter_trains():
    try:
        data = request.get_json()
        print("Received request to filter trains:", data)
       
        data = request.get_json()
        # Check for the required fields in the incoming data
        required_fields = ['departure_station', 'arrival_station', 'outbound_date', 'outbound_time', 'num_tickets', 'travel_class']
        for field in required_fields:
            if field not in data:
                return jsonify({"error": f"Missing required field: '{field}'"}), 400
        departure_station = data['departure_station']
        arrival_station = data['arrival_station']
        outbound_date = data['outbound_date']
        outbound_time = data['outbound_time']
        return_date = data.get('return_date')
        return_time = data.get('return_time')
        num_tickets = data['num_tickets']
        travel_class = data['travel_class']
        print("departure_station:", departure_station)
        print("arrival_station:", arrival_station)
        print("outbound_date:", outbound_date)
        print("outbound_time:", outbound_time)

        # Call the check_database_connection route to get the latest available_trains
        check_connection_response = requests.get('http://localhost:5000/check_database_connection').json()
        if 'error' in check_connection_response:
            return jsonify({"error": check_connection_response['error']}), 500

        # Extract available_trains from the response
        global available_trains
        available_trains = check_connection_response.get('data_from_database', [])
        filtered_trains = []
        print("Available Trains:", available_trains)

        for train in available_trains:
            if (
                train['departure_station'] == departure_station and
                train['arrival_station'] == arrival_station and
                (
                    (train['outbound_date'] == outbound_date and train['outbound_time'] == outbound_time)
                    or
                    (train['outbound_date'] == return_date and train['outbound_time'] == return_time)
                )
            ):
                seats_available = int(train[f'num_{travel_class.lower()}_seats'])
                if seats_available >= num_tickets:
                    flexible_fare = calculate_fare(int(train['fare_flexible_tickets']), travel_class) * num_tickets
                    non_flexible_fare = calculate_fare(int(train['fare_non_flexible_tickets']), travel_class) * num_tickets
                    print("Train ID:", train['id'])
                    # Create an entry for the outbound trip
                    outbound_trip = {
                        'id': f"{train['id']}",
                        'departure_station': train['departure_station'],
                        'arrival_station': train['arrival_station'],
                        'outbound_date': train['outbound_date'],
                        'outbound_time': train['outbound_time'],
                        'travel_class': travel_class,
                        'fares': {
                            'flexible': flexible_fare,
                            'non_flexible': non_flexible_fare
                        },
                        'trip_type': 'outbound'
                    }

                    filtered_trains.append(outbound_trip)

                    # If it's a return trip, find the ID and calculate fares for the return trip
                    if return_date and return_time:
                        return_trip_id = find_return_trip_id(departure_station, arrival_station, return_date, return_time, travel_class)

                        if return_trip_id:
                            return_flexible_fare, return_non_flexible_fare = calculate_return_fares(return_trip_id, num_tickets, travel_class)

                            if return_flexible_fare is not None and return_non_flexible_fare is not None:
                                return_trip = {
                                    'id': return_trip_id,
                                    'departure_station': arrival_station,
                                    'arrival_station': departure_station,
                                    'return_date': return_date,
                                    'return_time': return_time,
                                    'travel_class': travel_class,
                                    'fares': {
                                        'flexible': return_flexible_fare,
                                        'non_flexible': return_non_flexible_fare
                                    },
                                    'trip_type': 'return'
                                }

                                filtered_trains.append(return_trip)

        if not filtered_trains:
            return jsonify({"message": "No available trains."})

        return jsonify({"trains": filtered_trains})

    except Exception as e:
        return jsonify({"error": str(e)}), 500

@app.route('/book_ticket', methods=['POST', 'OPTIONS'])
def book_ticket():
    try:
        data = request.get_json()
        print("Received request to book a ticket:", data)
        
        data = request.get_json()
        username = data.get('username')
        password = data.get('password')
        outbound_trip_id = int(data['outbound_trip_id'])
        return_trip_id = int(data.get('return_trip_id'))
        travel_class = data['travel_class']
        ticket_type = data['ticket_type']
        num_tickets = int(data['num_tickets'])

        # Check for the required fields in the incoming data
        required_fields = ['outbound_trip_id', 'return_trip_id', 'travel_class', 'ticket_type']
        for field in required_fields:
            if field not in data:
                return jsonify({"error": f"Missing required field: '{field}'"}), 400
            
       


        # Call the check_database_connection route to get the latest available_trains
        check_connection_response = requests.get('http://localhost:5000/check_database_connection').json()
        if 'error' in check_connection_response:
            return jsonify({"error": check_connection_response['error']}), 500
        # Extract available_trains from the response
        global available_trains
        available_trains = check_connection_response.get('data_from_database', [])

      

        # Check if the outbound trip is available
        outbound_trip = next((train for train in available_trains if int(train['id'] )== outbound_trip_id), None)
        print("outbound_trip: ",outbound_trip )
        if not outbound_trip:
            return jsonify({"error": f"The outbound train with ID {outbound_trip_id} is not available"}), 404

        # Check if the return trip is available (if provided)
        return_trip = None
        if return_trip_id:
            return_trip = next((train for train in available_trains if int(train['id']) == return_trip_id), None)
            if not return_trip:
                return jsonify({"error": f"The return train with ID {return_trip_id} is not available"}), 404


    
        
        # Calculate the fare based on ticket type
        if ticket_type == 'flexible':
            fare = calculate_fare(int(outbound_trip['fare_flexible_tickets']), travel_class)*num_tickets
            if return_trip:
                fare += calculate_fare(int(return_trip['fare_flexible_tickets']), travel_class)*num_tickets
        elif ticket_type == 'not_flexible':
            fare = calculate_fare(int(outbound_trip['fare_non_flexible_tickets']), travel_class)*num_tickets
            if return_trip:
                fare += calculate_fare(int(return_trip['fare_non_flexible_tickets']), travel_class)*num_tickets
        else:
            return jsonify({"error": "Invalid ticket type"}), 400

         # Check if there is an existing reservation for the provided trip IDs and user credentials
        existing_reservation = []
        
        
        # Update the number of available seats after successful reservation
        outbound_trip[f'num_{travel_class.lower()}_seats'] = int(outbound_trip[f'num_{travel_class.lower()}_seats'])- num_tickets
        if return_trip!=None:
            return_trip[f'num_{travel_class.lower()}_seats'] = int(return_trip[f'num_{travel_class.lower()}_seats'])- num_tickets

        for reservation_id, reservation_details in reservations.items():
            
            if int(reservation_details['outbound_trip']['id']) == outbound_trip_id:
                if int(reservation_details['outbound_trip'][f'num_{travel_class.lower()}_seats']) < num_tickets:
                        return jsonify({"error": "No available seats for the outbound trip"})
                else :
                    # Update the number of available seats in the reservation
                    reservation_details['outbound_trip'][f'num_{travel_class.lower()}_seats'] = int(reservation_details['outbound_trip'][f'num_{travel_class.lower()}_seats']) - num_tickets
            if return_trip and int(reservation_details['return_trip']['id']) == return_trip_id:
                if int(reservation_details['return_trip'][f'num_{travel_class.lower()}_seats']) < num_tickets:
                        return jsonify({"error": "No available seats for the return trip"})
                else :
                    # Update the number of available seats in the reservation
                    reservation_details['return_trip'][f'num_{travel_class.lower()}_seats'] = int(reservation_details['return_trip'][f'num_{travel_class.lower()}_seats']) - num_tickets

        # Record the reservation 
        reservation_id = len(reservations) + 1
        reservation_details = {
            'outbound_trip': outbound_trip,
            'return_trip': return_trip,
            'travel_class': travel_class,
            'ticket_type': ticket_type,
            'total_fare': fare,
            'username': username,
            'password': password,

        }
        reservations[reservation_id] = reservation_details
        for reservation_id, reservation_details in reservations.items():
            existing_username = reservation_details.get('username')
            existing_password = reservation_details.get('password')
            if (
                    existing_username == username
                    and existing_password == password
                ):
                    existing_reservation.append({"reservation_id": reservation_id, "reservation_details": reservation_details})
        print("reservations" , reservations)

        return jsonify({"message": "Successful reservation", "reservation_id": reservation_id,"existing_reservations": existing_reservation})

    except Exception as e:
        return jsonify({"error": str(e)}), 500





# Run the application
if __name__ == '__main__':
    app.run(debug=True)
