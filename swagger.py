from flask import Flask, jsonify
from flask_restful import Api, Resource
from spyne import Integer, Unicode, Array, rpc,ComplexModel, Float
import requests
import csv
import html

from flask_cors import CORS

class TrainFares(ComplexModel):  
    flexible = Float
    non_flexible = Float

class Train(ComplexModel):
    id = Integer
    departure_station = Unicode
    arrival_station = Unicode
    date_time = Unicode
    available_seats = Integer
    travel_class = Unicode
    fares = TrainFares  
app = Flask(__name__)
CORS(app)  
api = Api(app)

# Load user data from CSV file
user_credentials = {}

def load_user_data():
    user_credentials = {}
    with open('users.csv', 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            username = row['username']
            password = row['password']
            user_credentials[username] = {
                'firstname': row['firstname'],
                'lastname': row['lastname'],
                'dateofbirth': row['dateofbirth'],
                'email': row['email'],
                'password': password
            }
    return user_credentials

# Authenticate user using both username and password
def authenticate_user(username, password):
    stored_user = user_credentials.get(username)

    if not (username and password):
        return print("Missing username or password")

    if stored_user is None or stored_user['password'] != password:
        return print("Unauthorized access")
    else:
        return True

# Load user credentials
user_credentials = load_user_data()

class TrainFilteringServiceResource(Resource):
    def post(self):
        
        @rpc(Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Integer, Unicode, _returns=Array(Train))
   
        def search_trains(self, username, password,departure_station, arrival_station, outbound_date, outbound_time, return_date, return_time, num_tickets, travel_class):
            if((authenticate_user(username, password)) == True): 
                try:
                    data = {
                            "departure_station": departure_station,
                            "arrival_station": arrival_station,
                            "outbound_date": outbound_date,
                            "outbound_time": outbound_time,
                            "return_date": return_date,
                            "return_time": return_time,
                            "num_tickets": num_tickets,
                            "travel_class": travel_class,
                            "username": username,
                            "password": password
                        }

                    rest_url = "http://127.0.0.1:5000/filter_trains"
                    response = requests.post(rest_url, json=data)

                    if response.status_code == 200:
                        result = response.json().get("trains", [])
                        return result
                    else:
                        return None
                except Exception as e:
                    print(f"Error during train search: {str(e)}")
                    return None
        pass

class TrainFilteringResource2(Resource):
    def post(self):
        @rpc(Unicode,Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, _returns=Unicode)
        def book_ticket(self, username, password, outbound_trip_id, return_trip_id, travel_class, ticket_type, num_tickets):
            # Authenticate the user
            if((authenticate_user(username, password)) == True): 
                try:

                    # Call REST service to update reservation
                    rest_url = "http://127.0.0.1:5000/book_ticket"
                    data = {

                        "outbound_trip_id": int(outbound_trip_id),
                        "return_trip_id": int(return_trip_id),
                        "travel_class": travel_class,
                        "ticket_type": ticket_type,
                        "num_tickets": int(num_tickets),
                        "username": username,
                        "password": password

                    }
                    response = requests.post(rest_url, json=data)
                    if response.status_code == 200:
                        result = response.json()
                        print("Result from REST service:", result)
                        if 'error' in result:
                            return f"Reservation error: {result['error']}"

                        reservation_id = result.get('reservation_id', '')
                        message = result.get('message', '')
                        existing_reservation = result.get('existing_reservation', None)

                        if existing_reservation:
                            reservation_details = existing_reservation.get('reservation_details', {})
                            outbound_trip_details = reservation_details.get('outbound_trip', {})
                            return_trip_details = reservation_details.get('return_trip', {})
                            return f"{message}. Details: {outbound_trip_details}.{return_trip_details}"

                        return f"{message}. Reservation ID: {reservation_id}"

                except Exception as e:
                    return f"Error during reservation: {str(e)}"
        pass
csv_file_path = 'train_data.csv'
def load_train_data():
    with open(csv_file_path, 'r') as file:
        reader = csv.DictReader(file)
        trains = list(reader)
        return trains

class TrainFilteringResource3(Resource):
    def get(self):
        def check_database_connection():
            # Load train data from CSV file
            global available_trains
            available_trains = load_train_data()

            return jsonify({"message": "Data loaded from CSV file", "data_from_database": available_trains})
        pass
                
api.add_resource(TrainFilteringServiceResource, '/filter_trains')
api.add_resource(TrainFilteringResource2, '/book_ticket')
api.add_resource(TrainFilteringResource3, '/check_database_connection')

@app.route('/openapi.json')
def openapi():
    openapi_spec = {
        'openapi': '3.0.0',
        'info': {
            'title': 'Train Filtering Service API',
            'version': '1.0.0',
        },
        'paths': {
            '/filter_trains': {
                'post': {
                    'summary': 'Search trains',
                    'requestBody': {
                        'content': {
                            'application/xml': {
                                'example': '''<?xml version="1.0" encoding="UTF-8"?>
                                <soap11env:Envelope xmlns:soap11env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="train_filtering">
                                    <soap11env:Body>
                                        <tns:search_trains>
                                            <tns:username>michaelw</tns:username>
                                            <tns:password>pass1234</tns:password>
                                            <tns:departure_station>Paris</tns:departure_station>
                                            <tns:arrival_station>Amsterdam</tns:arrival_station>
                                            <tns:outbound_date>2024-01-15</tns:outbound_date>
                                            <tns:outbound_time>08:00:00</tns:outbound_time>
                                            <tns:return_date>2024-02-14</tns:return_date>
                                            <tns:return_time>16:15:00</tns:return_time>
                                            <tns:num_tickets>1</tns:num_tickets>
                                            <tns:travel_class>first_class</tns:travel_class>
                                        </tns:search_trains>
                                    </soap11env:Body>
                                </soap11env:Envelope>'''
                            },
                        },
                    },
                    'responses': {
                        '200': {
                            'description': 'Successful response',
                            'content': {
                                'application/xml': {
                                    'example': '''<?xml version='1.0' encoding='UTF-8'?>
                                    <soap11env:Envelope xmlns:soap11env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="train_filtering">
                                        <soap11env:Body>
                                            <tns:search_trainsResponse>
                                                <tns:search_trainsResult>
                                                    <tns:Train>
                                                        <tns:id>1</tns:id>
                                                        <tns:departure_station>Paris</tns:departure_station>
                                                        <tns:arrival_station>Amsterdam</tns:arrival_station>
                                                        <tns:date_time>2024-01-15 08:00:00</tns:date_time>
                                                        <tns:travel_class>first_class</tns:travel_class>
                                                        <tns:fares>
                                                            <tns:flexible>300.0</tns:flexible>
                                                            <tns:non_flexible>200.0</tns:non_flexible>
                                                        </tns:fares>
                                                    </tns:Train>
                                                    <tns:Train>
                                                        <tns:id>14</tns:id>
                                                        <tns:departure_station>Amsterdam</tns:departure_station>
                                                        <tns:arrival_station>Paris</tns:arrival_station>
                                                        <tns:date_time>2024-02-14 16:15:00</tns:date_time>
                                                        <tns:travel_class>first_class</tns:travel_class>
                                                        <tns:fares>
                                                            <tns:flexible>240.0</tns:flexible>
                                                            <tns:non_flexible>160.0</tns:non_flexible>
                                                        </tns:fares>
                                                    </tns:Train>
                                                </tns:search_trainsResult>
                                            </tns:search_trainsResponse>
                                        </soap11env:Body>
                                    </soap11env:Envelope>'''
                                },
                            },
                        },
                    },
                },
            },
            '/book_ticket': {
                'post': {
                    'summary': 'Book a train ticket',
                    'requestBody': {
                        'content': {
                            'application/xml': {
                                'example': '''<?xml version="1.0" encoding="UTF-8"?>
                                <soap11env:Envelope xmlns:soap11env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="train_filtering">
                                    <soap11env:Body>
                                        <tns:book_ticket>
                                            <tns:username>michaelw</tns:username>
                                            <tns:password>pass1234</tns:password>
                                            <tns:outbound_trip_id>1</tns:outbound_trip_id>
                                            <tns:return_trip_id>14</tns:return_trip_id>
                                            <tns:travel_class>first_class</tns:travel_class>
                                            <tns:ticket_type>flexible</tns:ticket_type>
                                            <tns:num_tickets>1</tns:num_tickets>
                                        </tns:book_ticket>
                                    </soap11env:Body>
                                </soap11env:Envelope>'''
                            },
                        },
                    },
                    'responses': {
                        '200': {
                            'description': 'Successful response',
                            'content': {
                                'application/xml': {
                                    'example': '''<?xml version='1.0' encoding='UTF-8'?>
                                    <soap11env:Envelope xmlns:soap11env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="train_filtering">
                                        <soap11env:Body>
                                            <tns:book_ticketResponse>
                                                <tns:book_ticketResult>Successful reservation. Reservation ID: 2</tns:book_ticketResult>
                                            </tns:book_ticketResponse>
                                        </soap11env:Body>
                                    </soap11env:Envelope>'''
                                },
                            },
                        },
                    },
                },
            },
            '/check_database_connection': {
                'get': {
                    'summary': 'Check the connection status',
                    'responses': {
                        '200': {
                            'description': 'Successful response',
                            'content': {
                                'application/json': {
                                    'example': {
                                        "data_from_database": [
                                            {
                                                "arrival_station": "Amsterdam",
                                                "departure_station": "Paris",
                                                "fare_flexible_tickets": "150",
                                                "fare_non_flexible_tickets": "100",
                                                "id": "1",
                                                "num_first_class_seats": "20",
                                                "num_premium_class_seats": "30",
                                                "num_standard_class_seats": "50",
                                                "outbound_date": "2024-01-15",
                                                "outbound_time": "08:00:00",
                                                "total_tickets": "250"
                                            },
                                           
                                        ]
                                    }
                                },
                            },
                        },
                    },
                },
            },
        },
        
    }

    return jsonify(openapi_spec)

if __name__ == '__main__':
    CORS(app.run(debug=True, port=5000))
