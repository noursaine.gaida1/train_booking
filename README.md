# train_booking

Welcome to the Train Booking Service! This project provides both a RESTful API and a SOAP web service for managing train reservations. The application is built using Flask for the REST service and Spyne for the SOAP service. The services allow users to search for available trains, filter them based on various criteria, and book tickets.
## Description

**RESTful API**
- Features

    - [ ] Authentication: Secure your requests by authenticating with a username and password.
 
    - [ ]   Train Filtering: Search for available trains based on departure and arrival stations, dates, and other criteria.
    - [ ] Ticket Booking: Reserve tickets for outbound and/or return trips, specifying travel class, ticket type, and the number of tickets.

- Endpoints

    
    - [ ] /authenticate (POST): Authenticate users with provided credentials.
   
    - [ ]  /check_database_connection : Verify the connection to the train database.
   
    - [ ]  /filter_trains (POST): Filter available trains based on user preferences.
    - [ ] /book_ticket (POST): Book tickets for specific trips and passengers.

**SOAP Web Service**
- Features

    
    - [ ] Train Search: Utilize a SOAP service to search for available trains using a structured request.
    - [ ] Ticket Booking: Book tickets seamlessly through the SOAP service, integrated with the RESTful API.

- Methods

    
    - [ ] search_trains (SOAP): Search for trains based on specified criteria.
    
    - [ ] book_ticket (SOAP): Reserve tickets for outbound and/or return trips, specifying travel class, ticket type, and the number of tickets.
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.pedago.ensiie.fr/noursaine.gaida1/train_booking.git
git branch -M main
git push -uf origin main
```


***

## Installation
1. Make sure you have Flask installed:
   `pip install Flask`
2. If you don't have Postman installed, follow the installation steps here.

3. Install required Python packages:

    `pip install spyne Flask lxml psycopg2-binary swagger-ui-bundle`
4. For Swagger UI installation (Linux):

    
    - `wget https://github.com/swagger-api/swagger-ui/archive/master.zip`
    
    - `unzip master.zip`
    - `cd swagger-ui-master/dist`

    Replace the content of dist/index.html with the provided code in train_booking folder (train_booking/index_swagger.html).
5. run `python3 swagger.py`

6. Run Swagger UI:
    `python3 -m http.server`
    

7. Open your browser and navigate to http://localhost:8000 to see the Train Booking API documentation.


## Usage
You can test APIs with Postman (use the examples provided in the folder "test Postman")



## Authors and acknowledgment
Noursaine GAIDA 


