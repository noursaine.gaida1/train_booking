
from spyne import Application, ServiceBase, Integer, Unicode, Array, ComplexModel, rpc
import requests
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication
import csv
from datetime import datetime
from spyne.model.complex import ComplexModel
from spyne.model.primitive import Integer, Unicode, Float
from flask_cors import CORS
from flask import Flask
from flask_restful import Api, Resource




# Initialize Flask
app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
api= Api(app)

class TrainFares(ComplexModel):  
    flexible = Float
    non_flexible = Float

class Train(ComplexModel):
    id = Integer
    departure_station = Unicode
    arrival_station = Unicode
    date_time = Unicode
    available_seats = Integer
    travel_class = Unicode
    fares = TrainFares  

# Load user data from CSV file
user_credentials = {}

def load_user_data():
    user_credentials = {}
    with open('users.csv', 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            username = row['username']
            password = row['password']
            user_credentials[username] = {
                'firstname': row['firstname'],
                'lastname': row['lastname'],
                'dateofbirth': row['dateofbirth'],
                'email': row['email'],
                'password': password
            }
    return user_credentials

# Authenticate user using both username and password
def authenticate_user(username, password):
    stored_user = user_credentials.get(username)

    if not (username and password):
        return f"Missing username or password"

    if stored_user is None or stored_user['password'] != password:
        return f"Unauthorized access"
    else:
        return True

# Load user credentials
user_credentials = load_user_data()




class TrainFilteringService(ServiceBase):
   


    @rpc(Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Integer, Unicode, _returns=Array(Train))
   
    def search_trains(self, username, password,departure_station, arrival_station, outbound_date, outbound_time, return_date, return_time, num_tickets, travel_class):
        if((authenticate_user(username, password)) == True): 
            try:
                data = {
                        "departure_station": departure_station,
                        "arrival_station": arrival_station,
                        "outbound_date": outbound_date,
                        "outbound_time": outbound_time,
                        "return_date": return_date,
                        "return_time": return_time,
                        "num_tickets": num_tickets,
                        "travel_class": travel_class,
                        "username": username,
                        "password": password
                    }

                rest_url = "http://127.0.0.1:5000/filter_trains"
                print("rest url")
                response = requests.post(rest_url, json=data)

                if response.status_code == 200:
                    result = response.json().get("trains", [])
                    print(result)
                    return result
                else : 
                    # Generate SOAP response for unauthorized access
                    unauthorized_response = """
                        <?xml version='1.0' encoding='UTF-8'?>
                        <soap11env:Envelope xmlns:soap11env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="train_filtering">
                            <soap11env:Body>
                                <tns:search_trainsResponse>
                                    <tns:search_trainsResult>
                                        <tns:UnauthorizedAccess>Error: Unauthorized access. Please check your username and password.</tns:UnauthorizedAccess>
                                    </tns:search_trainsResult>
                                </tns:search_trainsResponse>
                            </soap11env:Body>
                        </soap11env:Envelope>
                    """
                    return unauthorized_response

            except Exception as e:
                print(f"Error during train search: {str(e)}")
               
       



    @rpc(Unicode,Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, _returns=Unicode)
 
    def book_ticket(self, username, password, outbound_trip_id, return_trip_id, travel_class, ticket_type, num_tickets):
    # Authenticate the user
    
        if((authenticate_user(username, password)) == True): 
            try:

                # Call REST service to update reservation
                rest_url = "http://127.0.0.1:5000/book_ticket"
                data = {

                    "outbound_trip_id": int(outbound_trip_id),
                    "return_trip_id": int(return_trip_id),
                    "travel_class": travel_class,
                    "ticket_type": ticket_type,
                    "num_tickets": int(num_tickets),
                    "username": username,
                    "password": password

                }
                response = requests.post(rest_url, json=data)
                if response.status_code == 200:
                    result = response.json()
                    print("Result from REST service:", result)
                    
                    
                    
                    reservation_id = result.get('reservation_id', '')
                    message = result.get('message', '')
                    existing_reservation = result.get('existing_reservations', None)
                    erreur = result.get('error', '')

                    if existing_reservation:
                    
                        reservation_details = existing_reservation[0].get('reservation_details', {})
                        outbound_trip_details = reservation_details.get('outbound_trip', {})
                        return_trip_details = reservation_details.get('return_trip', {})
                        return f"{message}.Reservation ID: {reservation_id}. Existing Reservation ID: {existing_reservation[0]['reservation_id']}.Details: {outbound_trip_details}.{return_trip_details}"
                    if erreur:
                        return f"{erreur}"

                    return f"{message}. Reservation ID: {reservation_id}"

            except Exception as e:
                return f"Error during reservation: {str(e)}"
        else : 
            result_user= authenticate_user(username, password)
            return result_user


            
class CORSMiddleware(object):
    def __init__(self, app, allow_origin='*'):
        self.app = app
        self.allow_origin = allow_origin

    def __call__(self, environ, start_response):
        if environ['REQUEST_METHOD'] == 'OPTIONS':
            # Handle OPTIONS request for CORS preflight
            headers = [
                ('Access-Control-Allow-Origin', self.allow_origin),
                ('Access-Control-Allow-Headers', 'Content-Type'),
                ('Access-Control-Allow-Methods', 'OPTIONS, POST, GET'),
            ]
            start_response('200 OK', headers)
            return [b'OK']

        def custom_start_response(status, headers, exc_info=None):
            headers.append(('Access-Control-Allow-Origin', self.allow_origin))
            headers.append(('Access-Control-Allow-Headers', 'Content-Type'))
            headers.append(('Access-Control-Allow-Methods', 'OPTIONS, POST, GET'))
            return start_response(status, headers, exc_info)

        return self.app(environ, custom_start_response)
    


class TrainFilteringResource(Resource):
    def post(self):
        
        @rpc(Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, Integer, Unicode, _returns=Array(Train))
    
        def search_trains(self, username, password,departure_station, arrival_station, outbound_date, outbound_time, return_date, return_time, num_tickets, travel_class):
            if((authenticate_user(username, password)) == True): 
                try:
                    data = {
                            "departure_station": departure_station,
                            "arrival_station": arrival_station,
                            "outbound_date": outbound_date,
                            "outbound_time": outbound_time,
                            "return_date": return_date,
                            "return_time": return_time,
                            "num_tickets": num_tickets,
                            "travel_class": travel_class,
                            "username": username,
                            "password": password
                        }

                    rest_url = "http://127.0.0.1:5000/filter_trains"
                   
                    response = requests.post(rest_url, json=data)

                    if response.status_code == 200:
                        result = response.json().get("trains", [])
                        
                        return result
                    else : 
                        
                        unauthorized_response = """
                            <?xml version='1.0' encoding='UTF-8'?>
                            <soap11env:Envelope xmlns:soap11env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="train_filtering">
                                <soap11env:Body>
                                    <tns:search_trainsResponse>
                                        <tns:search_trainsResult>
                                            <tns:UnauthorizedAccess>Error: Unauthorized access. Please check your username and password.</tns:UnauthorizedAccess>
                                        </tns:search_trainsResult>
                                    </tns:search_trainsResponse>
                                </soap11env:Body>
                            </soap11env:Envelope>
                        """
                        return unauthorized_response

                except Exception as e:
                    print(f"Error during train search: {str(e)}")

class TrainFilteringResource2(Resource):
    def post(self):
        @rpc(Unicode,Unicode, Unicode, Unicode, Unicode, Unicode, Unicode, _returns=Unicode)
        def book_ticket(self, username, password, outbound_trip_id, return_trip_id, travel_class, ticket_type, num_tickets):
            # Authenticate the user
            
                if((authenticate_user(username, password)) == True): 
                    try:

                        # Call REST service to update reservation
                        rest_url = "http://127.0.0.1:5000/book_ticket"
                        data = {

                            "outbound_trip_id": int(outbound_trip_id),
                            "return_trip_id": int(return_trip_id),
                            "travel_class": travel_class,
                            "ticket_type": ticket_type,
                            "num_tickets": int(num_tickets),
                            "username": username,
                            "password": password

                        }
                        response = requests.post(rest_url, json=data)
                        if response.status_code == 200:
                            result = response.json()
                            print("Result from REST service:", result)
                            
                            
                            
                            reservation_id = result.get('reservation_id', '')
                            message = result.get('message', '')
                            existing_reservation = result.get('existing_reservations', None)
                            erreur = result.get('error', '')
        
                            if existing_reservation:
                            
                                reservation_details = existing_reservation[0].get('reservation_details', {})
                                outbound_trip_details = reservation_details.get('outbound_trip', {})
                                return_trip_details = reservation_details.get('return_trip', {})
                                return f"{message}.Reservation ID: {reservation_id}. Existing Reservation ID: {existing_reservation[0]['reservation_id']}.Details: {outbound_trip_details}.{return_trip_details}"
                            if erreur:
                                return f"{erreur}"

                            return f"{message}. Reservation ID: {reservation_id}"

                    except Exception as e:
                        return f"Error during reservation: {str(e)}"
                else : 
                    result_user= authenticate_user(username, password)
                    return result_user


# Add Flask-RESTful resource to the Flask app
api.add_resource(TrainFilteringResource, '/filter_trains')
api.add_resource(TrainFilteringResource2, '/book_ticket')


# Create Spyne Application
application = Application([TrainFilteringService],
                          tns='train_filtering',
                          in_protocol=Soap11(validator='lxml'),
                          out_protocol=Soap11())

if __name__ == '__main__':
    from wsgiref.simple_server import make_server

    wsgi_app = WsgiApplication(application)
    wsgi_app = CORSMiddleware(wsgi_app)
    server = make_server('0.0.0.0', 8000, wsgi_app)
    print("SOAP service listening on http://localhost:8000")
    server.serve_forever()


